#include "eco-system/GameLoop.hpp"

#include <chrono>
#include <stdexcept>


namespace ecosystems::system {
    int GameLoop::run() {
        auto currentTime = std::chrono::high_resolution_clock::now();
        while(!m_should_exit) {
            auto newTime = std::chrono::high_resolution_clock::now();
            float deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(newTime - currentTime).count();
            currentTime = newTime;
            tick(deltaTime);
        }

        return EXIT_SUCCESS;
    }

    void GameLoop::tick(float p_delta_time) {
        m_loaded_scene->update(p_delta_time);
    }
}