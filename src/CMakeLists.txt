set(TARGET_INCLUDE_ROOT ${SYSTEM_INC_ROOT})
set(TARGET_SOURCE_ROOT ${SYSTEM_SRC_ROOT})

set_relative(TARGET_INCLUDES ${SYSTEM_INC_ROOT}
        EcoApiModule.h
        thirdparty/entt.hpp

        eco-system/components/BehaviorComponent.hpp
        eco-system/components/TransformComponent.hpp
        eco-system/Entity.hpp
        eco-system/Scene.hpp
        eco-system/GameLoop.hpp
        eco-system/Behavior.hpp
        )

set_relative(TARGET_SOURCES ${SYSTEM_SRC_ROOT}
        Entity.cpp
        Scene.cpp
        GameLoop.cpp
        Behavior.cpp
        )

project_add_library(${NAMESPACE}-system
        SOURCES ${TARGET_INCLUDES} ${TARGET_SOURCES}
        INCLUDES ${TARGET_EXTERNAL_INCLUDES}
        COMPILE_FLAGS ${TARGET_COMPILE_FLAGS}
        EXTERNAL_LIBS ${TARGET_EXTERNAL_LIBS}
        )
