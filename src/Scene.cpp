#include "eco-system/Scene.hpp"
#include "eco-system/Entity.hpp"
#include "eco-system/components/TagComponent.hpp"
#include "eco-system/components/TransformComponent.hpp"
#include "eco-system/components/BehaviorComponent.hpp"


namespace ecosystems::system {
    Entity Scene::createEntity(const std::string &p_tag) {
        Entity entity = { m_registry.create(), this };
        entity.addComponent<components::TransformComponent>();
        auto& tag = entity.addComponent<components::TagComponent>(p_tag);
        tag.tag = p_tag.empty() ? "Entity": p_tag;

        return entity;
    }

    void Scene::update(float p_delta_time) {
        m_registry.view<components::BehaviorComponent>().each([=](auto p_entity, auto& p_behavior) {
            if(!p_behavior.instance) {
                p_behavior.instantiateFunction();
                p_behavior.instance->m_entity = { p_entity, this };
                p_behavior.onCreateFunction(p_behavior.instance);
            }

            p_behavior.onUpdateFunction(p_behavior.instance, p_delta_time);
        });
    }
}