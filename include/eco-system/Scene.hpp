#ifndef ECOSYSTEMS_SCENE_HPP
#define ECOSYSTEMS_SCENE_HPP

#include <vector>

#include <EcoApiModule.h>
#include <thirdparty/entt.hpp>


namespace ecosystems::system {
    class GameLoop;
    class Entity;
    class Scene {
    public:
        ECO_API explicit Scene() = default;
        ECO_API ~Scene() = default;

        Scene(const Scene&) = delete;
        Scene& operator=(const Scene&) = delete;

        ECO_API Scene(Scene&&)=default;
        ECO_API Scene &operator=(Scene&&)=default;

        ECO_API virtual void mounted() {}
        ECO_API virtual void unmounted() {}
        ECO_API virtual void update(float p_delta_time);

        ECO_API Entity createEntity(const std::string& p_tag = std::string());
    protected:
        entt::registry m_registry;
        friend class Entity;
    };
}

#endif //ECOSYSTEMS_SCENE_HPP
