#ifndef ECOSYSTEMS_TAGCOMPONENT_HPP
#define ECOSYSTEMS_TAGCOMPONENT_HPP

#include <string>
#include <utility>


namespace ecosystems::system::components {
    struct TagComponent {
        std::string tag;

        TagComponent() = default;
        TagComponent(const TagComponent&) = default;
        explicit TagComponent(std::string  p_tag) : tag(std::move(p_tag)){}
    };
}

#endif // ECOSYSTEMS_TAGCOMPONENT_HPP && COMPILE_GRAPHICS_COMPONENTS
