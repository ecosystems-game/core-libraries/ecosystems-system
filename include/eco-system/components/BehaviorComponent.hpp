#ifndef ECOSYSTEMS_SCRIPTEDCOMPONENT_HPP
#define ECOSYSTEMS_SCRIPTEDCOMPONENT_HPP

#include <functional>

#include <eco-system/Entity.hpp>
#include <eco-system/Behavior.hpp>


namespace ecosystems::system {
    namespace components {
        struct BehaviorComponent {
            Behavior *instance = nullptr;

            std::function<void()> instantiateFunction;
            std::function<void()> destroyInstanceFunction;

            std::function<void(Behavior*)> onCreateFunction;
            std::function<void(Behavior*)> onDestroyFunction;
            std::function<void(Behavior*, float)> onUpdateFunction;

            template<typename T> void bind() {
                instantiateFunction = [&]() { instance = new T(); };
                destroyInstanceFunction = [&]() { delete (T *) instance; };

                onCreateFunction = [](Behavior *p_instance) { ((T *) p_instance)->onCreate(p_instance); };
                onUpdateFunction = [](Behavior *p_instance, float delta_time) { ((T *) p_instance)->onUpdate(p_instance, delta_time); };
                onDestroyFunction = [](Behavior *p_instance) { ((T *) p_instance)->onDestroy(p_instance); };
            }
        };
    } // ::components
} // ecosystems::system

#endif //ECOSYSTEMS_SCRIPTEDCOMPONENT_HPP
