#ifndef ECOSYSTEMS_GAMELOOP_HPP
#define ECOSYSTEMS_GAMELOOP_HPP

#include "EcoApiModule.h"
#include <memory>

#include "Entity.hpp"
#include "Scene.hpp"

namespace ecosystems::system {

    // The purpose of the gameloop class is to provide a container for the render loop iteration.
    // This includes calling Drawing to the screen, Handling input from controllers, and managing the windows
    // Associated to the application
    class GameLoop {
    public:
        ECO_API explicit GameLoop() = default;
        ECO_API ~GameLoop() = default;
        GameLoop(const GameLoop&) = delete;
        GameLoop &operator=(const GameLoop&) = delete;
        ECO_API virtual int run();
        ECO_API virtual void tick(float p_delta_time);
        ECO_API void load_scene(std::unique_ptr<system::Scene>&& p_game_view) {
            m_loaded_scene = std::move(p_game_view);
            m_loaded_scene->mounted();
        }

        ECO_API std::unique_ptr<system::Scene>& get_game_view() { return m_loaded_scene; }

        ECO_API bool get_should_exit() { return m_should_exit; }
        ECO_API void set_should_exit(bool p_should_exit) { m_should_exit = p_should_exit; }
    protected:
        bool m_should_exit = false;
        std::unique_ptr<system::Scene> m_loaded_scene = nullptr;
    };
}

#endif //ECOSYSTEMS_GAMELOOP_HPP
