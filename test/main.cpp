#include <eco-system/GameLoop.hpp>
#include "include/TestScene.hpp"


int main() {
    ecosystems::system::GameLoop gameLoop;
    gameLoop.load_scene(std::make_unique<ecosystems::system::test::scenes::TestScene>());

    try {
        return gameLoop.run();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}